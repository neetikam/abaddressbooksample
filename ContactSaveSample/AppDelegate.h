//
//  AppDelegate.h
//  ContactSaveSample
//
//  Created by shaik riyaz on 13/04/18.
//  Copyright © 2018 shaik riyaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

