//
//  ViewController.m
//  ContactSaveSample
//
//  Created by shaik riyaz on 13/04/18.
//  Copyright © 2018 shaik riyaz. All rights reserved.
//

#import "ViewController.h"
#import <AddressBook/AddressBook.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(createGroup) userInfo:nil repeats:NO];

}


-(void) createGroup
{
    ABAddressBookRef addressBook = ABAddressBookCreate();
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (granted) {
                CFErrorRef error = NULL;
                ABRecordRef worxMailGroup = ABGroupCreate();
                BOOL success = ABRecordSetValue(worxMailGroup,kABGroupNameProperty,(__bridge CFTypeRef) @"TestGroup2",&error);
                if (success) {
                    bool bStatus = ABAddressBookAddRecord(addressBook, worxMailGroup, &error);
                    if (bStatus) {
                        bStatus = ABAddressBookSave(addressBook, &error);
                        // bStatus is false and error also nil ..
                    }
                }
            }
        });
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
