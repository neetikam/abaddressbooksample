//
//  main.m
//  ContactSaveSample
//
//  Created by shaik riyaz on 13/04/18.
//  Copyright © 2018 shaik riyaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
